

package com.tsguild.foundations.BigLabs.InterestCalc;
import java.util.Scanner;
/**
 *
 * @author Dillon
 */
public class InterestCalculator {
    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner(System.in);
        
        double interest = 2.5;
        double yrInt, ttlInt = 0, ballance = 500;
        int year = 20, compound = 0;
        
        
        //!!! Current ballance * (1 + (2.5 /100)) !!!
        
        System.out.println("Enter how much you wish to invest");
        ballance = inpt.nextDouble();
        
        System.out.println("Enter how many years you will keep deposited");
        year = inpt.nextInt();
        
        System.out.println("Do you want to have it compound (1) quarterly or (2)monthly?");
        compound = inpt.nextInt();
        
        if(compound == 1){
            
            System.out.println("You have chosen quarterly");
            
            for (int i = 1; i <= year; i++){
                
                System.out.println("Starting principle for year " + i + ": $" + ballance + "\n");
                yrInt = ballance;
                
                for(int x = 1; x <= 4; x++){
                    
                    
                    ballance *= (1 + ((interest * x)/ 100));
                    
                    System.out.println("Quarter " + x + " earnings: $"  + ballance);
                    
                }
                
                ttlInt += ballance - yrInt;
                
                System.out.println("\n\nTotal year " + i + " interest earned: $" + (ballance - yrInt));
                System.out.println("Year " + i +" ending total: $" + ballance);
                System.out.println("Overall Interest earned: $" + ttlInt);
                System.out.println("\n\n");
                
            }
            
        } else if(compound ==2){
            
            System.out.println("You have chosen monthly");
            interest = 8.35;
            
            for (int i = 1; i <= year; i++){
                
                
                System.out.println("Starting principle for year " + i + ": $" + ballance + "\n");
                yrInt = ballance;
                
                for(int x = 1; x <= 12; x++){
                    
                    
                    ballance *= (1 + ((interest * x)/ 100));
                    
                    System.out.println("Total interest % " + interest * x);
                    System.out.println("Month " + x + " earnings: $"  + ballance);
                    
                }
                
                ttlInt += ballance - yrInt;
                
                System.out.println("\n\nTotal year " + i + " interest earned: $" + (ballance - yrInt));
                System.out.println("Year " + i +" ending total: $" + ballance);
                System.out.println("Overall Interest earned: $" + ttlInt);
                System.out.println("\n\n");
                
            }
            
            
        }
        
    }
    
}
