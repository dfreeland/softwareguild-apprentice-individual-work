

package com.tsguild.foundations.BigLabs.LuckySevens;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Dillon
 */
public class LuckySevens {
    private static int min = 1, max = 6;
    
    
    public static void main(String[] args) {
        
        Random rand = new Random();
        Scanner inpt = new Scanner(System.in);
        
        int money = 0, timesRolled = 0, maxMoney = 0, mMRoll = 0;
        int dieOne = 0, dieTwo = 0, dieSum = 0, muchMoney = 0, minusMoney = 0;
        
        
        System.out.println("Please enter the amount of money you have: ");
        money = inpt.nextInt();
        maxMoney = money;
        
        do{
            
            
            if(money > 0){
                
                dieOne = rand.nextInt((max - min) + 1) + min;
                dieTwo = rand.nextInt((max - min) + 1) + min;
                dieSum = dieOne + dieTwo;
                
                
                if(dieSum == 7){
                    
                    System.out.println("you rolled a " + dieSum);
                    System.out.println("Great! You win 4 dollars");
                    
                    money+= 4;
                    muchMoney+= 4;
                    
                    System.out.println("You have a total of $" + money);
                    
                } else {
                    
                    if(dieSum > 7){
                        
                        System.out.println("You rolled a " + dieSum
                                + ". That is higher than 7!");
                        money--;
                        minusMoney++;
                        
                    } else if(dieSum < 7){
                        System.out.println("You rolled a " + dieSum
                                + ". That is lower than 7!");
                        money--;
                        minusMoney++;
                        
                    }
                    
                    
                }
                
                
                
                timesRolled++;
                
                if(money > maxMoney){
                    maxMoney = money;
                    mMRoll++;
                }
                
            }
            
        }while(money != 0);
        
        
        System.out.println("\nYou rolled " + timesRolled + " times");
        System.out.println("You rolled " + mMRoll + " times with the most money");
        System.out.println("$"+ maxMoney +" was your highest amount owned.");
        System.out.println("$"+ muchMoney +" was how much you made while playing.");
        System.out.println("$"+ minusMoney +" was how much you lost while playing."); //feel the futility
        
        
        
        
        
        
        
        
        
        
    }
    
}
