

package com.tsguild.foundations.flowcontroll.ifs;
import java.util.Scanner;
/**
 *
 * @author softwareguild
 */
public class Trivia {
    
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        int ans = 0, chk = 0;
        
        System.out.println("FIRST QUESTION!\nWhat is the lowest level programming language?"
                + "\n 1 source code           2 assembly\n3 C# 4 Machine language");
        chk = inpt.nextInt();
        
        
        if(chk==4){
            ans++;
        }
        
        System.out.println("Your answer " + chk + "\nSECOND QUESTION\nWebsite Security CAPTCHA forms are descended from the work of?"
                + "\n 1 Grace Hopper      2 Alan Turing\n3 Charles Babbage       4 Larry Page");
        chk = inpt.nextInt();
        if(chk==2){
            ans++;
        }
        
        System.out.println("Your answer " + chk + "LAST QUESTION\nWhich of these schi-fi ships was once slated for a full-size replica in Las vegas?"
                + "\n 1 Serenity        2 The Battle Star Galactica\n3 The USS Enterprise        4 The Millenium Falcon");
       chk = inpt.nextInt();
        if(chk==4){
            ans++;
        }
        
        if(ans==3){
            System.out.println("You got them all!");
        } else if(ans==2){
            System.out.println("You got two right.");
        } else if(ans==1){
            System.out.println("You only got one right");
        } else {
            System.out.println("You are really bad at trivia.");
        }
        
    }
    
}
