

package com.tsguild.foundations.flowcontroll.ifs;

/**
 * Answers
 * 1.   else if is a logical operator that has to have the condition met
 *      if the first if is untrue
 * 2.   if you remove the else, then it will not be run if the first if 
 *      statement is falls, but will run if it is true
 * 
 * 
 *
 * @author Dillon Freeland
 */
public class SpaceRustlers {

    
    public static void main(String[] args) {
        
        
        int spaceShips = 10, aliens = 25, cows = 100;
        
        if(aliens > spaceShips){
            System.out.println("Vroom, vroom! Let us get going!");
        } else {
            
            System.out.println("There are not enough green guys to drive these ships!");
        }
        
        if(cows == spaceShips){
            
            System.out.println("Wow, way to plan ahead! Just enough room for all these walking hamburgers!");
        } else if(cows < spaceShips){
            
            System.out.println("Dangit! I do not know how we are going to fit all these cows in here!");
        } else { 
            
            System.out.println("Too many ships! not enough cows.");
        }
        
        
        if(aliens>cows){
            
            System.out.println("Hurrah! we have gotten the grub!");
        }
        
        if(aliens<=cows){
            
            System.out.println("Oh no the herd got restless and took over! looks like we are hamburger now!");
        }
        
        
        
        
        
        
        
        
        
    }
    
}
