

package com.tsguild.foundations.flowcontroll.ifs;
 import java.util.Scanner;
    
/**
 * Answer 
 * 1.   it does not understand it 
 * 2.   change the guess to all caps after the program gets the answer, and modify the if to use all caps
 *
 * @author Dillon
 */
public class KnockKnock {

   
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
        System.out.println("Knock knock guess who?");
        String guess = inpt.nextLine();
        
        if(guess.equals("Marty McFly")){
             System.out.println("Hey thats right! I am back!");
              System.out.println("...from the future!");
        } else {
         System.out.println("Dude, do I look like " + guess);
    }
        
        
        
    }
    
}
