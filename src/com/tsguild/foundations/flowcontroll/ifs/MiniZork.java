

package com.tsguild.foundations.flowcontroll.ifs;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class MiniZork {
    
    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner(System.in);
        
        
        System.out.println("You are standing in an open field west of a white house");
        
        System.out.println("With a boarded front door.");
        
        System.out.println("There is a small mailbox here.");
        
        System.out.println("Go to the house or open the mailbox?");
        String action = inpt.nextLine();
        
        if(action.equals("Open the mailbox")){
            
            System.out.println("You open the mailbox");
            
            System.out.println("It is very dark in there");
            
            System.out.println("Look inside or stick your hand in?");
            action = inpt.nextLine();
            
            if(action.equals("Stick your hand in")){
                
                System.out.println("You should not stick your hands in dark places.");
                
                System.out.println("You were eaten by a grue.");
                
            } else if(action.equals("Look inside")){
                
                System.out.println("You peer inside the mailbox, It is very very dark inside.");
                
                System.out.println("Keep looking or run away?");
                
                action = inpt.nextLine();
                
                if(action.equals("keep looking")){
                    
                    System.out.println("Turns out hanging out in dark areas is not a good idea");
                    
                    System.out.println("you were eaten by a grue.");
                    
                }else if(action.equals("run away")){
                    
                    System.out.println("You run away from the mailbox towards the house");
                    
                    System.out.println("you look foolish but it was wise that you did.");
                }
                
            }
            
            
            
        } else if(action.equals("go to the house")){
            
        System.out.println("you walk towards the house");
        
        System.out.println("It is very large with a big red door");
        System.out.println("Knock on the door or go inside?");
        action = inpt.nextLine();
        
        if(action.equals("go inside")){
            
        System.out.println("You go inside, turns out it was just your own house");
        
        System.out.println("and now you are safe.");
        
        } else if(action.equals("knock on the door")){
            
        System.out.println("You approach the the big red door");
        
        System.out.println("You raise your hand to give the door a loud knock");
        
        System.out.println("Suddenly it gets dark out and you get eaten by a grue.");
        }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
}
