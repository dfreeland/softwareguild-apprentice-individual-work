

package com.tsguild.foundations.flowcontroll.ifs;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class PickyEater {

    
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
         System.out.println("How many times has it been fried? #");
         int timesFried = inpt.nextInt();
         
          System.out.println("Does it have spinach in it? (y/n)");
          String hasSpinach = inpt.next();
          
          
          System.out.println("Is it covered in cheese? (y/n)");
          String hasCheese = inpt.next();
          
          
          System.out.println("How many pats of butter are on top? #");
          int butterPats = inpt.nextInt();
          
          System.out.println("Is it covered in chocolate? (y/n)");
         String chocolateCovered = inpt.next();
         
          System.out.println("Does it have a funny name? (y/n)");
         String funnyName = inpt.next();
         
         
          System.out.println("Is it broccoli? (y/n)");
         String broccoli = inpt.next();
         
         if(hasSpinach.equals("y")){
             
          System.out.println("There is no way that is going to get eaten!");
         }
         
         if(timesFried>=2 && timesFried<4){
             
          System.out.println("Oh that is like a deep fried snickers. That will be a hit!");
         }
         
         if(timesFried==2 && hasCheese.equals("y")){
             
          System.out.println("mmm yeah fried cheese doodles that will get eaten.");
         }
         
         if(broccoli.equals("y") && butterPats < 6 && hasCheese.equals("y")){
             
          System.out.println("As long as the green is covered by cheese it will happen!");
         }
         
         if(broccoli.equals("y")){
             
          System.out.println("Oh, green stuff like that might as well go in the bin.");
         }
         
         
         
         
         
         
         
    }
    
}
