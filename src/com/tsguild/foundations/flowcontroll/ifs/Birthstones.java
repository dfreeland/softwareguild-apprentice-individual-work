

package com.tsguild.foundations.flowcontroll.ifs;
import java.util.Scanner;
/**
 *
 * @author softwareguild
 */
public class Birthstones {

    
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
        int month = 0;
            System.out.println("Enter what month you want to know the birthstone of");
        month = inpt.nextInt();
        
        if(month==1){
            System.out.println("Garnet");
        } else if (month==2){
            System.out.println("Amethyst");
            
        } else if (month==3){
            System.out.println("Aquamarine");
            
        } else if (month==4){
            System.out.println("Diamond");
            
        } else if (month==5){
            System.out.println("Emerald");
            
        } else if (month==6){
            System.out.println("Pearl");
            
        } else if (month==7){
            System.out.println("Ruby");
            
        } else if (month==8){
            System.out.println("Peridot");
            
        } else if (month==9){
            System.out.println("Sapphire");
            
        } else if (month==10){
            System.out.println("Opal");
            
        } else if (month==11){
            System.out.println("Topaz");
            
        } else if (month==12){
            System.out.println("Turquoise");
            
        }
        
        
    }
    
}
