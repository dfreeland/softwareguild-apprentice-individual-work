

package com.tsguild.foundations.basics.core;

/**
 *
 * @author Dillon Freeland
 */
public class CommentingCode {

    
    public static void main(String[] args) {
        
        
        //Comments are written to explain the code in an easily understandable way
        //Basically for single lines anything after a // is considered a comment
        System.out.println("Normal code is compiled and runs...");
        System.out.println("Comments however..."); //do not execute
        
        //comments can be on their own line
        System.out.println("..."); //or they can share like this.
        
        //however if you put the // BEFORE a line of code
        //System.out.println("Then it is considered a comment, and will not execute!");
        /*
            
            This is a multi-lined comment!
            Named because, well it spans SO many lines!
        
        
        */
    }
    
}
