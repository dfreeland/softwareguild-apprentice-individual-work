

package com.tsguild.foundations.basics.core;

/**
 *
 * @author Dillon Freeland
 */
public class ProjectGutenburg {

    
    public static void main(String[] args) {
        
        System.out.println("Did you know that in 1440 (or thereabouts)");
        System.out.println("Johannes Gutenberg invented the printing press?");
        System.out.println("He started out as a goldsmith!");
        System.out.println("His invention made rapid and prolific typed volumes"
                            +" available for the first time\n ever...!");
        System.out.println("We are like a modern Gutenburg!");
        System.out.println("Printing vast amounts to the waiting console with ease!");
        
        
    }
    
}
