

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class MiniMadLibs {

    
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
        
        String noun1 = "", noun2 = "", adj1 = "", adj2 = "", pNn1 = "", pNn2 = "", pNn3 = "", verb1 = "", verb2 = "";
        int num;
        
        System.out.println("Give me a noun");
        noun1 = inpt.nextLine();
        
        
        System.out.println("give me an adjective ");
        adj1 = inpt.nextLine();
        
        
        System.out.println("Give me another noun");
        noun2 = inpt.nextLine();
        
        
        System.out.println("Give me a number");
        num = inpt.nextInt();
        
        
        System.out.println("give me another adjective");
        adj2 = inpt.nextLine();
        
        
        System.out.println("Give me plural noun");
        pNn1= inpt.nextLine();
        
        
        System.out.println("Give me another plural noun");
        pNn2= inpt.nextLine();
        
        
        System.out.println("Give me one last plural noun");
        pNn3= inpt.nextLine();
        
        
        System.out.println("Give me a verb present tense");
        verb1 = inpt.nextLine();
        
        
        System.out.println("Same verb but past tense");
        verb2 = inpt.nextLine();
        
        
        System.out.println(noun1 + " the " + adj1 + "frontier, These are the voyages of the "
                + "starship " + noun2 +". \nIts " + num + " year mission to explore strange "
                + adj2 + " " + pNn1 + ", to seek out " + adj2 + " " + pNn2 + " \nand "
                + adj2 + " " + pNn3 + ", to boldly " + verb1 + " where no one has " + verb2 + " before.");
        
        
    }
    
}
