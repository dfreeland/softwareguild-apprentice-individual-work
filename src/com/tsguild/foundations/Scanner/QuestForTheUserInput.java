
package com.tsguild.foundations.Scanner;
import java.util.Scanner;

/**
 *
 * @author Dillon Freeland
 */
public class QuestForTheUserInput {

    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner(System.in);
        
        String yourName, yourQuest;
        double velocityOfSwallow;
        
        
        System.out.println("What is your name?");
        yourName = inpt.nextLine();
        
        
        System.out.println("What is your quest?");
        yourQuest = inpt.nextLine();
        
        
        System.out.println("What is the velocity of an unladen swallow?");
        velocityOfSwallow = inpt.nextDouble();
        
        
        
        System.out.println("How do you know " + velocityOfSwallow + " is correct, " + yourName + ",");
        System.out.println("when you did not even know if the swallow was African or European?");
        System.out.println(" Maybe skip answering questions about birds and instead go  " + yourQuest);
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
}
