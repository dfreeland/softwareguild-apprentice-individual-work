

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author softwareguild
 */
public class AllTheTrivia {

    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner (System.in);
        
        String ans1 = "", ans2 = "", ans3 = "", ans4 = "";
        
        System.out.println("1,024 Gigabytes is equal to one what? ");
        ans1 = inpt.nextLine();
        
        
        System.out.println("In our solar system, which is the only planet that rotates clockwise? ");
        ans2 = inpt.nextLine();
        
        
        System.out.println("The largest volcano ever discovered in our solar system is located on which planet?");
        ans3 = inpt.nextLine();
        
        
        System.out.println("What is the most abundant element in the earth's atmosphere? ");
        ans4 = inpt.nextLine();
        
        
        
        
        System.out.println("Wow, 1,024 gigabytes is a " + ans3
                            + "\nI did not know  that the largest ever volcano was discovered on " + ans1
                            + "!\nThat is amazing that " + ans2 + " is the most abundant element in the atmosphere...\n"
                            + ans4 + " is the only planet that rotates clockwise, neat!");
    }
    
}
