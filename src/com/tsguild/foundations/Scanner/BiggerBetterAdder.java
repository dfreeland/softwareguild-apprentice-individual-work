

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class BiggerBetterAdder {

    
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
        int nOne = 0, nTwo = 0, nThr = 0, sum;
        
        
        System.out.println("Input your first number to add");
        nOne = inpt.nextInt();
        
        
        System.out.println("Input your second number to add");
        nTwo = inpt.nextInt();
        
        
        System.out.println("Input your third number to add");
        nThr = inpt.nextInt();
        
        
        System.out.println(nOne + " + " + nTwo + " + " + nThr);
        
        System.out.println(nOne+nTwo+nThr);
        
        sum = nOne+nTwo+nThr;
        
        System.out.println(sum + " is the sum of the numbers you added.");
    }
    
}
