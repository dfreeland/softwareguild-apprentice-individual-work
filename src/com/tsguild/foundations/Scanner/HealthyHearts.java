

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class HealthyHearts {

    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner(System.in);
        
        double age, max, tar;
        
        System.out.println("What is your age?");
        age = inpt.nextInt();
        
        max = (220 - age);
        tar = (max * .85) - 50;
        
        System.out.println("A healthy heart rate should be:   " + max
        + "\n Target heart rate should be: " + tar);
        
        
    }
    
}
