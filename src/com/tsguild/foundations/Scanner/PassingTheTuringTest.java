

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author Dillon Freeland
 */
public class PassingTheTuringTest {

    
    public static void main(String[] args) {
        
        Scanner inpt = new Scanner(System.in);
        
        String name ="", color = "", fruit = "";
        int num;
        
        
        System.out.println("Hello there!");
        
        System.out.println("What is your name?");
        name = inpt.nextLine();
        
        
        
        System.out.println("Hi, " + name + ". What is your favorite color?");
        color = inpt.nextLine();
        
        
        System.out.println("Huh, " + color +"? Mine is electric lime.");
        
        System.out.println("I really like limes. They are my favofite fruit, too."
                + "\nWhat is your favorite fruit," + name +"?");
        fruit = inpt.nextLine();
        
        System.out.println("Really? " + fruit + "? That is wild!");
        
        System.out.println("Speaking of favorites, what is your favorite number?");
        num = inpt.nextInt();
        
        System.out.println(num + " is a really cool number. Mine is -7.\n"
                + "Did you know that " + num + " * -7 is " + (num*(-7)) 
                + "? That is a cool number too! \nWell, thanks for talking to me, " + name + "!");
        
        
        
    }
    
}
