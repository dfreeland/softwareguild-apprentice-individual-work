

package com.tsguild.foundations.Scanner;
import java.util.Scanner;
/**
 *
 * @author softwareguild
 */
public class DoItBetter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner inpt = new Scanner(System.in);
        
        int miles, dogs, lang;
        
        System.out.println("How many miles can you run?");
        miles = inpt.nextInt();
        
        
        System.out.println("How many hotdogs can you eat?");
        dogs = inpt.nextInt();
        
        
        System.out.println("How many languages do you know?");
        lang = inpt.nextInt();
        
        
        System.out.println("Oh really? I can run " + (1 +(miles * 2)) + " Miles");
        
        System.out.println("I can also eat " + (1 +(dogs * 2)) + " hotdogs");
        
        System.out.println("And I speak " + (1 +(lang * 2)) + " languages.");
        
    }
    
}
