

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class InABucket {

    
    public static void main(String[] args) {
        
        
        //You can declare all KINDS of variables.
        String walrus;
        double piesEaten;
        float weightOfTeacupPig = 3;
        int grainsOfSand = 1000000000;
        
        //but declaring them just sets up the space for data
        //to use the variable,, you have to initialize it first!
        walrus = "Sir Leeroy Jenkins III";
        piesEaten = 42.1;
        
        System.out.println("Meet my pet Walrus, " + walrus);
        System.out.print("He was a bit hungry today, and ate this many pies: ");
        System.out.println(piesEaten);
        
        System.out.println("The teacup pig weighs" + weightOfTeacupPig + "Pounds.");
        System.out.println("There are more than " + grainsOfSand + " grains of sand "
                + "on my floor in my house at all times.");
        
        
        
    }
    
}
