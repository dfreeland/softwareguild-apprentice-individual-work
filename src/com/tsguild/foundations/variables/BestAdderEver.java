

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class BestAdderEver {

    
    public static void main(String[] args) {
        
        int nOne = 7, nTwo = 93, nThr = -2, sum;
        
        System.out.println(nOne + " + " + nTwo + " + " + nThr);
        
        System.out.println(nOne+nTwo+nThr);
        
        sum = nOne+nTwo+nThr;
        
        System.out.println(sum);
        
    }
    
}
