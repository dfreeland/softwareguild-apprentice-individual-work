

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class MenuOfChampions {
    
    public static void main(String[] args) {
        
        
        String pizza = "Cheese Pizza", nachos = "Zesty Nachos", omelette = "Omelette du fromage";
        double pzaPr = 12.50, nchPr = 5.99, omlPr = 8.99;
        
        
        System.out.println("");
        System.out.println("*^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^*");
        System.out.println("                 Welcome to Just Feed Me Something!");
        System.out.println("                 Today's Menu consists the following");
        System.out.println("*^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^* *^*\n");
        System.out.println("                 " + pizza + "           $" + pzaPr);
        System.out.println("                 " + nachos + "           $" + nchPr);
        System.out.println("                 " + omelette + "    $" + omlPr);
        
    }
    
}
