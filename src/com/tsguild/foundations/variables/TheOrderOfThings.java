

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class TheOrderOfThings {

    
    public static void main(String[] args) {
        
        double number;
        String opinion, size, age, shape, color, origin, material, purpose;
        String noun;
        
        number = 5.0;
        opinion = "Awesome!";
        size = "Mini";
        age = "kind of new";
        shape = "cuboid";
        color = "taupe";
        origin = "Crab Nebula";
        material = "Soggy pizza box";
        purpose = "smelly";
        
        noun = "Coffee";
        
        // using the + with strings, does not add it to the concatenates! (sticks them together)
        
        System.out.println(number + " " + age +  " " + size +  " " + shape 
        +  " " + noun +  " " + color +  " " + origin +  " " + purpose 
                +  " " + material +  " " + opinion);
        
        
        
    }
    
}
