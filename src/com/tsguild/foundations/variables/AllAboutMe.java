

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class AllAboutMe {
    
    public static void main(String[] args) {
        
        String Name = "Dillon", Food = "Hot Pastrami";
        boolean gnocchi = false;
        int age = 6, pets = 2;
        
        System.out.println("My name is " + Name + ".");
        System.out.println("I have " + pets + " pets.");
        System.out.println("My favorite food is " + Food +".");
        System.out.println("If I said that I knew what gnocchi is, that would be " + gnocchi +".");
        System.out.println("When I was " + age + " years old I learned how to whistle");
        
    }
    
}
