

package com.tsguild.foundations.variables;

/**
 *
 * @author Dillon Freeland
 */
public class MoreBucketsMoreFun {

    /**
     * ANSWER
     * 
     * Using the -- operator to show that the dog ate 1 butterfly
     * 
     * We initialized bugs to equal butterflies plus beetles before we subtracted the butterfly
     * 
     */
    public static void main(String[] args) {
        
        //Declare ALL THE THINGS
        //(Usually it is a good idea to declare them at the beginning of the program)                                                                                                               I declare where i want
        
        int butterflies, beetles, bugs;
        String color, size, shapre, thing;
        double number;
        
        //now give a couple of them some values
        butterflies = 2;
        beetles = 4;
        
        bugs = butterflies+beetles;
        
        System.out.println("There are only " + butterflies + " butterflies,");
        System.out.println("but " + bugs + "total.");
        
        
        System.out.println("Uh oh, my dog ate one.");
        butterflies--;
        System.out.println("Now there are only " + butterflies + " butterflies left.");
        System.out.println("But still " + bugs + " bugs left, wait a minute!!!");
        System.out.println("Maybe I just counted wrong the first time...");
        
    }
    
}
